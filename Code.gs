var sheetURL =
    "https://docs.google.com/spreadsheets/d/1AA6MTcR-JI55u45OmBagOecZDXGRdzb2yBIQ1MyidJU/edit#gid=1786666575";

function doGet(e) {
    e['sheetURL'] = sheetURL;
    var params = JSON.stringify(e);
    //return HtmlService.createHtmlOutput(params);

    var template = HtmlService.createTemplateFromFile("index");

    return template.evaluate();
}

var sheetURL =
    "https://docs.google.com/spreadsheets/d/1AA6MTcR-JI55u45OmBagOecZDXGRdzb2yBIQ1MyidJU/edit#gid=1786666575";

function loadCatalogue() {
    var ss = SpreadsheetApp.openByUrl(sheetURL);
    var metroOrdersNamedRange = ss.getRangeByName('MetroItems');

    var metroItemsData = metroOrdersNamedRange.getValues();

    // Remove the header and convert it to id, Title before sending
    var catalogueData = metroItemsData.slice(1).map((item) => {
        return {
            'id': item[0],
            'title': item[2],
            'selected': false
        };
    });

    //Logger.log(JSON.stringify(catalogueData));
    return JSON.stringify(catalogueData);
}

function loadFlatNumbers() {
    var ss = SpreadsheetApp.openByUrl(sheetURL);
    var FlatNumberListNamedRange = ss.getRangeByName('FlatNumberList');

    var FlatNumbersData = FlatNumberListNamedRange.getValues();
    //Logger.log(JSON.stringify(FlatNumbersData));
    return JSON.stringify(FlatNumbersData.flat());
}

function saveOrderData(flatNumber, selectedIDs) {
    var ss = SpreadsheetApp.openByUrl(sheetURL);
    var dataSheet = ss.getSheetByName("Metro Orders");
    var metroOrdersNamedRange = ss.getRangeByName('MetroOrders');

    Logger.log([metroOrdersNamedRange.getNumRows(), metroOrdersNamedRange.getNumColumns()]);

    var metroOrdersData = metroOrdersNamedRange.getValues();
    /*  
        //Logger.log(metroItems['MetroItems']);
        dataSheetNamedRanges.forEach((range) => {
            if (range.getName() === 'MetroOrders')
                metroOrdersData = range.getRange().getValues();
        });
    */
    // Find the column corresponding to the flat Number
    var headers = metroOrdersData[0];
    var flatColumnNumber = headers.indexOf(flatNumber);

    if (flatColumnNumber === -1) {
        return JSON.stringify({
            'query':
            {
                'flatNumber': flatNumber,
                'items': selectedIDs
            },
            'flat': 'NotFound',
            'column': flatColumnNumber,
            'result': 'FAILED',
            'message': 'Wrong Flat Number'
        });
    }
    var rowData = [];
    metroOrdersData.forEach((row, index) => {
        if (selectedIDs.indexOf(row[0]) !== -1) {
            rowData.push({ 'index': index, 'ID': row[0], 'name': row[2] });
            //Logger.log("Getting Cell: ", index+1, flatColumnNumber+1, { 'index' : index, 'item' : row[0]});
            //Logger.log(metroOrdersNamedRange.getCell(index+1, flatColumnNumber+1));
            metroOrdersNamedRange.getCell(index + 1, flatColumnNumber + 1).setValue(1);
        }
    });

    return JSON.stringify({
        'query':
        {
            'flatNumber': flatNumber,
            'items': selectedIDs
        },
        'items': selectedIDs,
        'flat': headers[flatColumnNumber],
        'column': flatColumnNumber,
        'rowData': rowData,
        'result': 'OK',
        'message': 'Order Saved Successfully. Thank you for shopping.'
    });
}

function itemAdded(itemInfo) {
    return JSON.stringify(itemInfo);
}

function include(filename) {
    return HtmlService.createHtmlOutputFromFile(filename).getContent();
}